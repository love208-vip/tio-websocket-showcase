var ws_protocol = 'ws'; // ws 或 wss
var ip = '127.0.0.1'
var port = 9326

var heartbeatTimeout = 5000; // 心跳超时时间，单位：毫秒
var reconnInterval = 1000; // 重连间隔时间，单位：毫秒

var binaryType = 'arraybuffer'; // 'blob' or 'arraybuffer';//arraybuffer是字节
var handler = new DemoHandler()

var tiows
var queryString = 'group=test&name=web'
var groupId
var userId

function initWs() {
  var param = null
  tiows = new tio.ws(ws_protocol, ip, port, queryString, param, handler, heartbeatTimeout, reconnInterval, binaryType)
  tiows.connect()
}



function send() {
  var msg = document.getElementById('textId').value;
  var chatReqBody = new proto.com.im.common.packets.ChatReqBody();
  chatReqBody.setTime(new Date().getTime());
  chatReqBody.setText(msg);
  chatReqBody.setType(1);
  chatReqBody.setGroup(groupId);
  var bytes = chatReqBody.serializeBinary();
  tiows.send(bytes);
  // tiows.send(msg)
}

function clearMsg() {
  document.getElementById('contentId').innerHTML = ''
}

function setQueryString() {
  groupId = document.getElementById('groupId').value;
  userId = document.getElementById('userId').value;
  queryString = 'group=' + groupId + '&name=' + userId;
  console.log("queryString>" + queryString);
}

function closeToDo() {
  tiows.close();
  console.log("tiows.close");
}
//发送退出房间指令
function sendExit() {
  var chatReqBody = new proto.com.im.common.packets.ChatReqBody();
  chatReqBody.setTime(new Date().getTime());
  chatReqBody.setText("exit");
  var bytes = chatReqBody.serializeBinary();
  tiows.send(bytes);
  console.log("send exit");
}